#k_azcapotzalco
##d_tlalhuactan ###################################
###c_tezcoco
6 = {	#Azcapotzalco
	culture = tepanec
	religion = cuecuexist 
	holding = city_holding
}
58 = {	#Tlatilco
	holding = none
}
59 = {	#Atizapan
	holding = city_holding
}
###c_tlacopan
5 = {	#Tlacopan
	culture = tepanec
	religion = cuecuexist 
	holding = city_holding
}
29 = {	#Cuajimalpa
	holding = none
}
###c_coyohuacan
27 = {	#Coyohuacan
	culture = tepanec
	religion = cuecuexist 
	holding = city_holding
}
28 = {	#Chapultepec
	holding = church_holding
}
##d_cauahtitlan ###################################
###c_cauahtitlan
63 = {	#Cauahtitlan
	culture = acolhua
	religion = cuecuexist 
	holding = city_holding
}
60 = {	#Toltitlan
	holding = none
}
64 = {	#Teohuilloyocan
	holding = city_holding
}
###c_tenayocan
30 = {	#Tenayocan
	culture = acolhua
	religion = cuecuexist 
	holding = city_holding
}
32 = {	#Ecatepec
	holding = city_holding
}
31 = {	#Tepeacac
	holding = church_holding
}
##d_colhuacan ###################################
###c_colhuacan
11 = {	#Colhuacan
	culture = acolhua
	religion = cuecuexist 
	holding = city_holding
}
24 = {	#Ixtapallucan
	holding = none
}
###c_tenochtitlan
1 = {	#Tenochtitlan
	culture = mexica
	religion = huitzilopochtlian 
	holding = city_holding
}
###c_chimalhuacan
74 = {	#Chimalhuacan
	culture = acolhua
	religion = tloquenahuaque 
	holding = city_holding
}
76 = {	#Huexotla
	culture = acolhua
	religion = tloquenahuaque 
	holding = city_holding
}